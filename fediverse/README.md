# Fediverse

The Fediverse is a bunch of different social networks connected together.

The are connected with a number of different protocols. Specifically, Pleroma and Mastodon can talk to each other via ActivityPub.

I prefer Pleroma's tech because it is an Elixir Phoenix app, whereas Mastodon is a Rails app.