def setup():
    size(2000, 1000)
    
    for i in range(0, 200):
        pushMatrix()
        translate(random(0, width), random(0, height))
        mandala()
        popMatrix()
        
    save("background.png")


def mandala():
    
    n = random(70, 100)
    theta = 0
    radius = random(50, 300)
    a = 200
    
    noStroke()

    colors = [color(240, 148, 148,a), color(93, 92, 255,a), color(188, 255, 195,a), color(255, 248, 188,a), color(243, 188, 255,a)]

    for i in range(0, int(n)):
        fill(colors[i%len(colors)])
        r2 = random(10, 150)
    
        x = cos(theta) * radius
        y = sin(theta) * radius
        ellipse(x, y, r2, r2)
        theta = theta + (TAU / n)
