# Languages

The world around us is made of code. We can manipulate that world in ways that preserve and defend our freedom.

A hacker has a list of their favorite go to languages for a variety of reasons. Here is mine.

# Ruby

Expressive Object Oriented scripting language. Language is very flexible, and easy to read. One of the first languages I fell in love with.
I enjoy writing web scrapers with Nokogiri and small servers with Sinatra.
Useful flavors are Opal. Combined with webpack, any Node.js project can be glossed over with Opal code.

# Elixir

Expressive functional programming language that is heavily influenced by Ruby. Code runs on the Erlang VM. Very concurrent, very reliable, much wow.
Network programming is one of the sweet spots for this language because of its Erlang lineage. OTP is... OP! lol

# Clojure

Modern LISP running on top of the JVM (as well as other platforms). Lisp is really cool and Clojure makes use of it really well. Rich Hickey is a genius. Data oriented philosophy makes it great at working with data. I like to use it for any of my data science related work. Very good at ETF tasks.

# Elm

Purely functional programming language for writing front ends. Made me actually enjoy writing front ends. The static typing, combined with the immutable data structures makes the software very reliable (no runtime errors!).

# Rust

High level systems programming language. Performance competes with C/C++ but the language provides a lot more power. Compiler is designed for safety.
Great for building reliable programs.

# Processing

Fun API for creating visuals with programs. Supports Java, JS, and Python. Used to make graphic designs or animations.
Great for teaching. How I got started with programming so it is near to my heart.