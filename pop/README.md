# pop

The pop-theme for GNOME is a really slick theme created by System76.

https://pop.system76.com/docs/install-pop-theme/

Here is how to install it on a fresh Ubuntu 18.04. Make sure you have Universal open. Look in Software & Updates to turn it on. Or else pop-theme will not install.

```
sudo apt-get update
sudo apt-get upgrade # very important

sudo add-apt-repository ppa:system76/pop
sudo apt update
sudo apt install pop-theme
```
