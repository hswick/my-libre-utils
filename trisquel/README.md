# Trisquel

<p align="center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Logo-Trisquel.svg/2000px-Logo-Trisquel.svg.png"/>
</p>

Trisquel is a Libre derivative of Ubuntu. Trisquel releases involve stripping the Ubuntu releases of all proprietary software.

Trisquel uses the MATE desktop environment. The web browser is ABrowser. You can also install IceCat which is a very Libre browser that prevents proprietary JavaScript from running. ABrowser has most normal functioning on websites, while IceCat has a cuter logo.

IceCat seems to be very slow. So it might be a fun project to see how we can improve the speed.
Possible idea: Write the LibreJS extension in Rust.

Trisquel uses MATE as the desktop environment by default.

## Contributing to Trisquel

The Gitlab project - https://devel.trisquel.info/groups/trisquel

Issues - https://trisquel.info/en/project/issues/trisquel

## Using Gnome

Trisquel uses the MATE desktop environment, which personally isn't my favorite. I would like to use GNOME. It is free software, you should be able to do what you want.

Trisquel includes a very minimal version that we can install GNOME on top of. Here is a great article that describes how to do it https://trisquel.info/en/wiki/installing-gnome-shell

I'll summarize the commands here:

Grab the ISO:
```
wget https://mirror.fsf.org/trisquel-images/trisquel-sugar_8.0_amd64.iso
```

Run the ISO however you want.

Then install gnome with `sudo apt-get install gnome`

## Running Trisquel with QEMU-KVM

QEMU is a Libre virtualization tool that we can use to emulate running an operating system. This is a great tool to try out ideas on OS installs.

Install:
```
sudo apt-get install qemu qemu-kvm
```

```
mkdir trisquel
mv *.iso trisquel
cd trisquel
qemu-img create -f qcow2 virtualtrisquel.img 30G
kvm -hda virtualtrisquel.img -cdrom *.iso -m 2048 -net nic -net user -soundhw all
# You only need to run the last command once
kvm -soundhw all -m 2048 -hda virtualtrisquel.img
```

After installing GNOME, I seemed to have had a problem booting it back up.

Don't include the soundhw if you don't want sound

# Pop Theme

There are a variety of themes that can be used for GNOME. My favorite so far is the Pop Theme created by System76.

Here is how to get it:

```
sudo add-apt-repository ppa:system76/pop
sudo apt update
sudo apt install pop-theme
```

# Make your own Trisquel
https://trisquel.info/en/wiki/customizing-trisquel-iso

# trisquel_rake

Set of Rake utilities for developing Trisquel instances and removing non free software

Possible to do Ruby interface for Trisquel packages